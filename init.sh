#!/usr/bin/env bash
export CUSTOM_EA_INITIALIZED=1

export CUSTOM_EA_SYNC=$CUSTOM_EA_DIR/SYNC

export CUSTOM_EA_SSH_USER_HOST=dev@ea
export CUSTOM_EA_SSH_DIR=custom-ea

alias cd-custom-ea="cd $CUSTOM_EA_DIR"

echo Custom EA Platform initialized
