#!/usr/bin/env bash
if [ -z "$CUSTOM_EA_INITIALIZED" ]; then echo Custom EA need to be initialized first; exit; fi

rsync -avhz --delete -e ssh \
    --exclude 'node_modules/' \
    --exclude '.gitignore' \
    --exclude '.DS_Store' \
    SYNC/ $CUSTOM_EA_SSH_USER_HOST:./$CUSTOM_EA_SSH_DIR/
