#!/usr/bin/env bash
if [ -z "$CUSTOM_EA_INITIALIZED" ]; then echo Custom EA need to be initialized first; exit; fi

## Compile Custom Components
echo
echo '### COMPILE Custom Components'
cd $CUSTOM_EA_DIR/custom-components
node src/build.js

## Compile Custom Mailer
echo
echo '### COMPILE Custom Mailer'
cd $CUSTOM_EA_DIR/custom-mailer
env GOOS=linux GOARCH=amd64 go build -o $CUSTOM_EA_SYNC/custom/custom-mailer
