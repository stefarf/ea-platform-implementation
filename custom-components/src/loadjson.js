const fs = require('fs');
const path = require('path');
const config = require('./config');

exports.create = function () {
    const asyncComponents = [];
    const bundleFiles = fs.readdirSync(config.bundlesDir);
    bundleFiles.forEach(fileName => {
        if (fileName.slice(-3) !== '.js') return;

        const componentName = fileName.slice(0, -3);
        asyncComponents.push({
            url: `/custom/${componentName}.js`,
            components: [componentName]
        });
    });

    const jsonFull = path.resolve(config.bundlesDir, 'load.json');
    const jsonText = JSON.stringify({asyncComponents}, null, 2);
    // console.log(jsonText);
    fs.writeFileSync(jsonFull, jsonText, 'utf8');
};