const fs = require('fs');

const bundle = require('./bundle');
const config = require('./config');
const entry = require('./entry');
const loadjson = require('./loadjson');

const componentFiles = fs.readdirSync(config.componentsDir);
const entryFiles = fs.readdirSync(config.entriesDir);
let bundleFiles = [];
try {bundleFiles = fs.readdirSync(config.bundlesDir);} catch (e) {}

const {entries, vueFiles} = entry.create(componentFiles, entryFiles);
bundle.compile(entries, vueFiles, bundleFiles, () => {
    loadjson.create();
});