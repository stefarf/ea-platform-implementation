const path = require('path');

const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = {
    // entry: {
    //     name: fullPath
    // },
    output: {
        // path: path.resolve(__dirname, '../bundles'),
        filename: '[name].js',
        library: 'bundle',
        libraryTarget: 'this'
    },

    //
    // Below are typical configuration
    //

    mode: 'production',
    optimization: {
        minimize: true,
        minimizer: [new UglifyJsPlugin()]
    },
    plugins: [
        new VueLoaderPlugin()
    ],
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },

            // this will apply to both plain `.js` files
            // AND `<script>` blocks in `.vue` files
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },

            // this will apply to both plain `.css` files
            // AND `<style>` blocks in `.vue` files
            {
                test: /\.css$/,
                use: [
                    'vue-style-loader',
                    'css-loader'
                ]
            }
        ]
    }
};