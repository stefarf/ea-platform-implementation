const fs = require('fs');
const path = require('path');
const webpack = require('webpack');

const config = require('./config');
const webpackConfig = require('./webpack.config');

exports.compile = function (entries, vueFiles, bundleFiles, done) {
    const mark = {};

    // Set default mark for bundle files
    bundleFiles.forEach(fileName => {
        const bundleFull = path.resolve(config.bundlesDir, fileName);
        if (bundleFull.slice(-3) === '.js') mark[bundleFull] = {marked: false};
    });

    // Scan each component, create bundle files if needed, and mark
    for (const componentName in entries) {
        const vueFull = vueFiles[componentName];
        const bundleFull = path.resolve(config.bundlesDir, `${componentName}.js`);

        if (!mark[bundleFull]) {
            // create bundle, leave entries as it is
        } else {
            // check modified time to decide whether to create bundle or not
            const vuemtime = fs.statSync(vueFull).mtimeMs;
            const bundlemtime = fs.statSync(bundleFull).mtimeMs;
            if (vuemtime <= bundlemtime) {
                // bundle exists, delete from entries
                delete entries[componentName];
            } else {
                // create bundle, leave entries as it is
            }
        }

        mark[bundleFull] = {marked: true};
    }

    // Delete unmarked entry files
    for (const bundleFull in mark)
        if (!mark[bundleFull].marked) {
            console.log(`Delete bundle file ${bundleFull}`);
            fs.unlinkSync(bundleFull);
        }

    if (!Object.keys(entries).length) return;

    // Create bundle files
    console.log('Create bundle:', entries);
    webpackConfig.entry = entries;
    webpackConfig.output.path = config.bundlesDir;
    webpack(webpackConfig, (err, stats) => {
        if (err || stats.hasErrors()) {
            console.error(err);
            return;
        }
        console.log('Bundles compiled');
        done();
    });

};