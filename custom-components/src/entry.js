const fs = require('fs');
const path = require('path');
const config = require('./config');

function vueToComponentName(s) {
    let out = '';
    for (let i = 0; i < s.length; i++) {
        if (i === 0) {
            out += s[i].toLowerCase();
        } else {
            if ('A' <= s[i] && s[i] <= 'Z') {
                out += '-' + s[i].toLowerCase();
            } else if ('a' <= s[i] && s[i] <= 'z') {
                out += s[i];
            }
        }
    }
    return out;
}

function createEntryScript(entryFull, vueName, componentName) {
    console.log(`Create entry script ${entryFull}`);
    const content = `import ${vueName} from '../src/components/${vueName}.vue';export const components = {'${componentName}': ${vueName}};`;
    fs.writeFileSync(entryFull, content, 'utf8');
}

// Create required entry files
exports.create = function (componentFiles, entryFiles) {
    const mark = {};
    const entries = {};
    const vueFiles = {};

    // Set default mark for entry files
    entryFiles.forEach(fileName => {
        const entryFull = path.resolve(config.entriesDir, fileName);
        if (entryFull.slice(-3) === '.js') mark[entryFull] = {marked: false};
    });

    // Scan component files and create entry files if needed, and mark
    componentFiles.forEach(fileName => {
        const vueName = fileName.split('.')[0];
        const componentName = `custom-${vueToComponentName(vueName)}`;
        const entryFull = path.resolve(config.entriesDir, `${componentName}.js`);
        if (!mark[entryFull]) createEntryScript(entryFull, vueName, componentName);
        mark[entryFull] = {marked: true, componentName};
        vueFiles[componentName] = path.resolve(config.componentsDir, fileName);
    });

    // Delete unmarked entry files
    for (const entryFull in mark) {
        if (!mark[entryFull].marked) {
            console.log(`Delete entry file ${entryFull}`);
            fs.unlinkSync(entryFull);
        } else {
            const componentName = mark[entryFull].componentName;
            entries[componentName] = entryFull;
        }
    }

    return {entries, vueFiles};
};