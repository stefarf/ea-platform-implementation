const path = require('path');

exports.componentsDir = path.resolve(__dirname, 'components');
exports.entriesDir = path.resolve(__dirname, '../entries');
exports.bundlesDir = path.resolve(__dirname, '../../SYNC/web');
