package main

import (
	"bitbucket.org/stefarf/iferr"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

var config struct {
	Port        string
	HttpTimeout int `yaml:"http_timeout"`
}

const configFile = "/apps/custom/config/mailer.yaml"

func init() {
	b, err := ioutil.ReadFile(configFile)
	iferr.Exit(err, "error read mailer config file")
	iferr.Exit(yaml.Unmarshal(b, &config), "error parse mailer config file")
}
