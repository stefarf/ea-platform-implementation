package main

import (
	"bitbucket.org/stefarf/httptool/httpsvr"
	"bitbucket.org/stefarf/httptool/restsvr"
	"bitbucket.org/stefarf/iferr"
	"fmt"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"os"
	"time"
)

const ver = "v1"

func main() {
	mux := http.NewServeMux()
	mux.Handle("/"+ver+"/", http.StripPrefix("/"+ver, apiHandlers()))
	fmt.Println("Custom Mailer is running on port:", config.Port)
	iferr.Exit(
		httpsvr.ListenAndServe(
			":"+config.Port, "", "",
			time.Second*time.Duration(config.HttpTimeout),
			mux), "")
}

func apiHandlers() http.Handler {
	api := httprouter.New()
	api.POST("/send-email", restsvr.By(handlerPushJob))
	return api
}

func handlerPushJob(ctx *restsvr.Context) {
	var inp struct {
		To      []string
		Subject string
		HTML    string
	}
	err := ctx.ReadJson(&inp)
	if err != nil {
		ctx.StatusBadRequest(err.Error())
		return
	}
	fmt.Println(inp.To, inp.Subject, inp.HTML)
	fmt.Println("")
	os.Exit(1)
}
