#!/usr/bin/env bash

go get bitbucket.org/stefarf/httptool/httpsvr
go get bitbucket.org/stefarf/httptool/restsvr
go get bitbucket.org/stefarf/httptool/restcli
go get bitbucket.org/stefarf/iferr
go get github.com/julienschmidt/httprouter
go get gopkg.in/yaml.v2
go get github.com/satori/go.uuid

cd $CUSTOM_EA_DIR/custom-components/ && npm i

cd $CUSTOM_EA_SYNC/custom/lib/db-lib/ && npm i
cd $CUSTOM_EA_SYNC/custom/lib/http-lib/ && npm i
cd $CUSTOM_EA_SYNC/custom/lib/jwt-lib/ && npm i
cd $CUSTOM_EA_SYNC/custom/tm-config/ && npm i
