#!/usr/bin/env bash

IMG_NAME=stefarf/ea-platform:1
RUN_NAME=custom-ea
RUN_PORT=443

docker stop $RUN_NAME 2>/dev/null
docker rm   $RUN_NAME 2>/dev/null
docker stop ea 2>/dev/null # To stop previously named ea
docker rm   ea 2>/dev/null #

docker run \
    --name $RUN_NAME \
    -d --restart always \
    -p $RUN_PORT:2000 \
    -v $PWD/custom:/apps/custom \
    -v $PWD/web:/apps/web/custom \
    -e EA_CONFIG_MODE=prod \
    $IMG_NAME run-svc
docker system prune -f
