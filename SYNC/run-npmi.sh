#!/usr/bin/env bash

IMG_NAME=stefarf/ea-platform:1

docker pull $IMG_NAME
docker stop npmi 2>/dev/null
docker run \
    --name npmi \
    -it --rm \
    -v $PWD/custom:/apps/custom \
    $IMG_NAME npmi.sh
