const jwt = require('jsonwebtoken');

exports.signHS256 = function (data, key, options) {
    return new Promise((resolve, reject) => {
        jwt.sign(data, key, {
            algorithm: 'HS256',
            ...options
        }, (err, token) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(token);
        });
    });
};

exports.verifyHS256 = function (token, key, options) {
    return new Promise((resolve, reject) => {
        jwt.verify(token, key, {
            algorithm: 'HS256',
            ...options
        }, (err, decoded) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(decoded);
        });
    });
};