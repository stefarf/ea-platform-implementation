function groupBy(rows, grpFrom, grpTo, memTo) {
    const grpmem = {};
    rows.forEach(row => {
        const val = row[grpFrom];
        if (!grpmem[val]) grpmem[val] = [];
        grpmem[val].push(row);
    });
    const rst = [];
    Object.keys(grpmem).forEach(val => {
        const obj = {};
        obj[grpTo] = val;
        obj[memTo] = grpmem[val];
        rst.push(obj);
    });
    return rst;
}

// multi = [
//     {
//         table,
//         sections: [
//             {
//                 section,
//                 list: [
//                     {title, subtitle, url}, ...
//                 ]
//             }, ...
//         ]
//     }, ...
// ]
function fromRows(rows) {
    rows = groupBy(rows, '$table', 'table', 'sections');
    rows.forEach(obj => {
        obj.sections = groupBy(obj.sections, '$section', 'section', 'list');
        obj.sections.forEach(obj => {
            const newList = [];
            obj.list.forEach(item => {
                newList.push({title: item.$title, subtitle: item.$subtitle, url: item.$url});
            });
            obj.list = newList;
        });
    });
    return rows;
}

module.exports = {fromRows};