const mysql = require('mysql');
const fs = require('fs');

class DB {
    constructor(cfg) {
        this.con = mysql.createConnection(cfg);

        // This is to suppress error ECONNRESET
        this.con.on('error', err => {
            console.log('Suppress mysql error');
            console.log(err);
        });
    }

    end() {
        return new Promise((resolve, reject) => {
            console.log('END DB CONNECTION');
            this.con.end(err => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve();
            });
        });
    }

    query(sql, values) {
        return new Promise((resolve, reject) => {
            // console.log('QUERY');
            this.con.query(sql, values, (err, result) => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve(result);
            });
        });
    }

    begin() {
        return new Promise((resolve, reject) => {
            console.log('BEGIN TRANSACTION');
            this.con.beginTransaction(err => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve();
            });
        });
    }

    commit() {
        return new Promise((resolve, reject) => {
            console.log('COMMIT TRANSACTION');
            this.con.commit(err => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve();
            });
        });
    }

    rollback() {
        return new Promise(resolve => {
            console.log('ROLLBACK TRANSACTION');
            this.con.rollback(() => {
                resolve();
            });
        });
    }

}

function readFile(filename) {
    return new Promise((resolve, reject) => {
        fs.readFile(filename, 'utf8', (err, data) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(data);
        });
    });
}

exports.connect = async function (opt) {
    const config = {
        host: opt.host,
        port: opt.port,
        user: opt.user,
        password: opt.password,
        database: opt.database,
        dateStrings: true
    };
    if (opt.cerFile) config.ssl = {ca: await readFile(opt.cerFile)};
    return new DB(config);
};