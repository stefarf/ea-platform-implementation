# Run API and BG

```js
// run api
const svr = require('taskmgmt-svr');
svr.setConfig(require('./config'));
svr.runAPI();

// run bg
const svr = require('taskmgmt-svr');
svr.setConfig(require('./config'));
svr.runBG();
```