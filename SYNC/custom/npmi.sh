#!/usr/bin/env bash

cd /apps/custom/lib/db-lib/ && npm i
cd /apps/custom/lib/http-lib/ && npm i
cd /apps/custom/lib/jwt-lib/ && npm i
cd /apps/custom/tm-config/ && npm i
