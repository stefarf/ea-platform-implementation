#!/usr/bin/env bash

## List all remote tags
#git ls-remote --tags origin >origin-tags.txt
#
## List all local tags
#git tag >local-tags.txt
#
## To delete local and remote tag
#git tag -d tag-name && git push origin :refs/tags/tag-name
#
## Tag and push to remote
#git tag tag-name && git push --tags
