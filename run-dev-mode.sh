#!/usr/bin/env bash
if [ -z "$CUSTOM_EA_INITIALIZED" ]; then echo Custom EA need to be initialized first; exit; fi

docker stop custom-ea 2>/dev/null
docker run \
    --name custom-ea \
    -it --rm \
    -p 8080:2000 \
    -v $CUSTOM_EA_SYNC/custom:/apps/custom \
    -v $CUSTOM_EA_SYNC/web:/apps/web/custom \
    -e EA_CONFIG_MODE=dev \
    stefarf/ea-platform:1 run-svc
docker system prune -f
